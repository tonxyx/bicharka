<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Define some useful constants
 */
define('BASE_DIR', dirname(__DIR__));
define('APP_DIR', BASE_DIR . '/apps');

$di = new FactoryDefault();

// Specify routes for modules
// More information how to set the router up https://docs.phalconphp.com/en/latest/reference/routing.html
$di->set('router', function () {

  $router = new Router();

  $router->setDefaultModule('frontend');

  $router->add(
    "/login",
    array(
      'module'     => 'backend',
      'controller' => 'session',
      'action'     => 'login'
    )
  );

  $router->add(
    "/admin/:controller/:action",
    array(
      'module' => 'backend',
      'controller' => 1,
      'action'     => 2
    )
  );

  return $router;
});

try {

  // Create an application
  $application = new Application($di);

  // Register the installed modules
  $application->registerModules(
    array(
      'frontend' => array(
        'className' => 'Bicharka\Frontend\Module',
        'path'      => APP_DIR . '/frontend/Module.php',
      ),
      'backend'  => array(
        'className' => 'Bicharka\Backend\Module',
        'path'      => APP_DIR . '/backend/Module.php',
      )
    )
  );

  // Handle the request
  echo $application->handle()->getContent();

} catch (\Exception $e) {
  echo $e->getMessage();
}
