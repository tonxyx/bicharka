<?php

namespace Bicharka\Backend\Controllers;

/**
 * Display the default index page
 */
class IndexController extends ControllerBase {

  public function initialize (){
    $this->view->setTemplateBefore('admin');
  }

  /**
 * Default action. Set the public layout (layouts/admin.phtml)
 */
  public function indexAction () {
    $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
  }

}
