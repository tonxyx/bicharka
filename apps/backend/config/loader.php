<?php

return $frontendLoaderNamespaces = [
    'Bicharka\Backend\Models'      => $this->config->application->modelsDir,
    'Bicharka\Backend\Controllers' => $this->config->application->controllersDir,
    'Bicharka\Backend\Forms'       => $this->config->application->formsDir,
];

