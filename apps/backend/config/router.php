<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();

$router->setDefaultModule('backend');

//$router->add('/confirm/{code}/{email}', array(
//    'controller' => 'user_control',
//    'action' => 'confirmEmail'
//));
//
$router->add('/admin/', array(
  'controller' => 'index',
  'action' => 'index'
));

return $router;
