<?php
namespace Bicharka\Backend\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Bicharka\Models\Profile;

class UserForm extends Form {

  public function initialize ($entity = null, $options = null) {

    // In edition the id is hidden
    if (isset($options['edit']) && $options['edit']) {
      $id = new Hidden('id');
    } else {
      $id = new Text('id');
    }
    $this->add($id);

    // First name
    $firstName = new Text('firstName');
    $firstName->setLabel('First Name');
    $firstName->addValidators([
      new PresenceOf([
        'message' => 'The first name is required',
      ]),
    ]);
    $this->add($firstName);

    // Last name
    $lastName = new Text('lastName');
    $lastName->setLabel('Last Name');
    $lastName->addValidators([
      new PresenceOf([
        'message' => 'The last name is required',
      ]),
    ]);
    $this->add($lastName);

    // Job
    $job = new Text('job');
    $job->setLabel('Job');
    $this->add($job);

    // Email
    $email = new Text('email', array(
      'placeholder' => 'Email',
    ));
    $email->addValidators(array(
      new PresenceOf(array(
        'message' => 'The e-mail is required',
      )),
      new Email(array(
        'message' => 'The e-mail is not valid',
      )),
    ));
    $this->add($email);

    $this->add(new Select('profileId', Profile::find('active = 1'), array(
      'using' => array(
        'id',
        'name',
      ),
      'useEmpty' => true,
      'emptyText' => '...',
      'emptyValue' => '',
    )));

    $this->add(new Select('banned', array(
      1 => 'Yes',
      0 => 'No',
    )));

    $this->add(new Select('suspended', array(
      1 => 'Yes',
      0 => 'No',
    )));

    $this->add(new Select('active', array(
      1 => 'Yes',
      0 => 'No',
    )));
  }
}
