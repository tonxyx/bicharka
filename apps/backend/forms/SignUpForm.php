<?php
namespace Bicharka\Backend\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class SignUpForm extends Form {

  public function initialize ($entity = null, $options = null) {
    // First name
    $firstName = new Text('firstName');
    $firstName->setLabel('First Name');
    $firstName->addValidators([
      new PresenceOf([
        'message' => 'The first name is required',
      ]),
    ]);
    $this->add($firstName);

    // Last name
    $lastName = new Text('lastName');
    $lastName->setLabel('Last Name');
    $lastName->addValidators([
      new PresenceOf([
        'message' => 'The last name is required',
      ]),
    ]);
    $this->add($lastName);

    // Email
    $email = new Text('email');
    $email->setLabel('E-Mail');
    $email->addValidators(array(
      new PresenceOf(array(
        'message' => 'The e-mail is required',
      )),
      new Email(array(
        'message' => 'The e-mail is not valid',
      )),
    ));
    $this->add($email);

    // Password
    $password = new Password('password');
    $password->setLabel('Password');
    $password->addValidators(array(
      new PresenceOf(array(
        'message' => 'The password is required',
      )),
      new StringLength(array(
        'min' => 12,
        'messageMinimum' => 'Password is too short. Minimum 12 characters',
      )),
      new Confirmation(array(
        'message' => 'Password doesn\'t match confirmation',
        'with' => 'confirmPassword',
      )),
    ));
    $this->add($password);

    // Confirm Password
    $confirmPassword = new Password('confirmPassword');
    $confirmPassword->setLabel('Confirm Password');
    $confirmPassword->addValidators(array(
      new PresenceOf(array(
        'message' => 'The confirmation password is required',
      )),
    ));
    $this->add($confirmPassword);

    // Terms & conditions
    $terms = new Check('terms', array(
      'value' => 1,
    ));
    $terms->setLabel('I accept terms and conditions');
    $terms->addValidator(new Identical(array(
      'value' => 1,
      'message' => 'Terms and conditions must be accepted',
    )));
    $this->add($terms);

    // CSRF
    $csrf = new Hidden('csrf');
    $csrf->addValidator(new Identical(array(
      'value' => $this->security->getSessionToken(),
      'message' => 'CSRF validation failed',
    )));
    $csrf->clear();
    $this->add($csrf);

    // Sign Up
    $this->add(new Submit('Sign Up', array(
      'class' => 'btn btn-success',
    )));
  }

  /**
   * Prints messages for a specific element
   */
  public function messages ($name) {
    if ($this->hasMessagesFor($name)) {
      $messages = '';
      foreach ($this->getMessagesFor($name) as $message) {
        $messages .= $message . "<br>";
      }
      $this->flash->error($messages);
    }
  }
}
