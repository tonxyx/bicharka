<?php

// Use composer autoloader to load vendor classes
require_once __DIR__ . '/../../../vendor/autoload.php';

return $commonLoaderNamespaces = [
  'Bicharka\Common\Library\Acl' => $this->config->application->libraryCommonDir . 'Acl/',
  'Bicharka\Common\Library\Auth' => $this->config->application->libraryCommonDir . 'Auth/',
  'Bicharka\Common\Library\Mail' => $this->config->application->libraryCommonDir . 'Mail/',
  'Bicharka\Common\Models' => $this->config->application->modelsCommonDir,
];
