<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;

/**
 * FailedLogin
 *
 * This model registers unsuccessful login registered and non-registered users have made
 */
class FailedLogin extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $userId;

  /**
   * @var string
   */
  public $ipAddress;

  /**
   * @var integer
   */
  public $attempted;

  public function initialize () {
    $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
      'alias' => 'users',
    ));
  }
}
