<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;

/**
 * RememberToken
 *
 * Stores the remember me token
 */
class RememberToken extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var integer
   */
  public $userId;

  /**
   * @var string
   */
  public $token;

  /**
   * @var string
   */
  public $userAgent;

  /**
   * @var integer
   */
  public $createdAt;

  public function initialize () {
    $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
      'alias' => 'users',
    ));
  }

  /**
   * Before create the user assign a password
   */
  public function beforeValidationOnCreate () {
    // Timestamp the confirmaton
    $this->createdAt = time();
  }
}
