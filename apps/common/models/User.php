<?php

namespace Bicharka\Common\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 * Bicharka\Models\User
 *
 * All the users registered in the application
 */
class User extends Model {

  /**
   * @var integer
   */
  public $id;

  /**
   * @var string
   */
  public $firstName;

  /**
   * @var string
   */
  public $lastName;

  /**
   * @var string
   */
  public $job;

  /**
   * @var string
   */
  public $email;

  /**
   * @var string
   */
  public $password;

  /**
   * @var integer
   */
  public $mustChangePassword;

  /**
   * @var string
   */
  public $profileId;

  /**
   * @var string
   */
  public $profileImage;

  /**
   * @var integer
   */
  public $banned;

  /**
   * @var integer
   */
  public $suspended;

  /**
   * @var integer
   */
  public $active;

  public function initialize () {
    $this->belongsTo('profileId', __NAMESPACE__ . '\Profile', 'id', [
      'alias' => 'profiles',
      'reusable' => true,
    ]);

    $this->hasMany('id', __NAMESPACE__ . '\SuccessLogin', 'userId', [
      'alias' => 'successLogins',
      'foreignKey' => [
        'message' => 'User cannot be deleted because he/she has activity in the system',
      ],
    ]);

    $this->hasMany('id', __NAMESPACE__ . '\PasswordChange', 'userId', [
      'alias' => 'passwordChanges',
      'foreignKey' => [
        'message' => 'User cannot be deleted because he/she has activity in the system',
      ],
    ]);

    $this->hasMany('id', __NAMESPACE__ . '\ResetPassword', 'userId', [
      'alias' => 'resetPasswords',
      'foreignKey' => [
        'message' => 'User cannot be deleted because he/she has activity in the system',
      ],
    ]);
  }

  /**
   * Before create the user assign a password
   */
  public function beforeValidationOnCreate () {
    if (empty($this->password)) {
      // Generate a plain temporary password
      $tempPassword = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(12)));

      // The user must change its password in first login
      $this->mustChangePassword = 1;

      // Use this password as default
      $this->password = $this->getDI()
        ->getSecurity()
        ->hash($tempPassword);
    } else {
      // The user must not change its password in first login
      $this->mustChangePassword = 0;
    }

    // The account must be confirmed via e-mail
    $this->active = 0;

    // The account is not suspended by default
    $this->suspended = 0;

    // The account is not banned by default
    $this->banned = 0;
  }

  /**
   * Send a confirmation e-mail to the user if the account is not active
   */
  public function afterSave () {
    if ($this->active === 0) {
      $emailConfirmation = new EmailConfirmation();
      $emailConfirmation->userId = $this->id;
      if ($emailConfirmation->save()) {
        $this->getDI()
          ->getFlash()
          ->notice('A confirmation mail has been sent to ' . $this->email);
      }
    }
  }

  /**
   * Validate that emails are unique across users
   */
  public function validation () {
    $this->validate(new Uniqueness([
      "field" => "email",
      "message" => "The email is already registered",
    ]));

    return $this->validationHasFailed() != true;
  }
}
