<?php

use Phalcon\Config;
use Phalcon\Logger;

return [
    'application' => [
        'controllersDir' => APP_DIR . '/frontend/controllers/',
        'modelsDir' => APP_DIR . '/frontend/models/',
        'formsDir' => APP_DIR . '/frontend/forms/',
        'viewsDir' => APP_DIR . '/frontend/views/',
        'cacheDir' => APP_DIR . '/frontend/cache/',
        'baseUri' => '/',
    ],
    'logger' => [
        'path'     => APP_DIR . '/frontend/logs/',
        'format'   => '%date% [%type%] %message%',
        'date'     => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'app_frontend.log',
    ]
];
